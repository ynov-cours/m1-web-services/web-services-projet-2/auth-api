const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  login: {
    type: String,
    max: 255,
    required: true
  },
  password: {
    type: String,
    required: true,
    max: 1024,
    min: 4
  },
  roles: {
    type: Array,
    default: ['ROLE_USER']
  },
  status: {
    type: String,
    default: "open"
  },
  created_at: {
    type: Date,
    default: new Date(),
  },
  updated_at: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model('User', userSchema);