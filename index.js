const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const app = express();
const PORT = 3000;

dotenv.config();
//Import routes
const authRoutes = require('./routes/auth');
const tokenRoutes = require('./routes/token');


//Connect to DB
mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true }, () => {
  console.log("Connected to DB !");
});

//middlewares
app.use(express.json());
app.use('/api/account', authRoutes);
app.use('/api', tokenRoutes);

app.listen(PORT, () => {
  console.log("Server listening on " + PORT);
})