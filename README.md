# Web Services - Projet 2 | Auth API

###### Projet créé par [Enzo Avagliano](https://gitlab.com/EloxFire)

> Le fichier .env du projet est inclu dans le répertoire GIT pour des raisons de pratique.
> Il n'apparraitrait en aucun cas dans un projet réel. Aussi, tous identifiants donnés dans
> ce projet seront détruits dans 30 jours.

> Si vous avez des soucis de connexion avec la base de donnée, merci de me contacter sur ce mail
> <a href="mailto:enzo.avagliano@ynov.com">enzo.avagliano@ynov.com</a>

1. Clonez le projet
   
   ```bash
   git clone https://gitlab.com/ynov-cours/m1-web-services/web-services-projet-2/auth-api.git
   ```

2. Installez les dépendances
   
    ```bash
   npm i
   ```

3. Lancez le projet
   
    ```bash
   npm start
   ```