const router = require('express').Router();
const bcrypt = require('bcryptjs');
const User = require('../model/User');
const { registerValidation } = require('./validation');


//CREATION COMPTE USER
router.post('/', async (req, res) => {
  //DATA VALIDATION
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // CHEKING IF USER EXISTS
  const login = await User.findOne({ login: req.body.login });
  console.log(login);
  if (login) return res.status(400).send("Login already used...");

  //HASHING PASSWORD
  const salt = await bcrypt.genSalt(10);
  const hashedPass = await bcrypt.hash(req.body.password, salt);

  //CREATING A NEW USER
  const user = new User({
    login: req.body.login,
    password: hashedPass,
    roles: req.body.roles,
    status: req.body.status,
    created_at: new Date(),
    updated_at: new Date()
  });

  try {
    const savedUser = await user.save();
    res.status(200).send(savedUser);
  } catch (error) {
    res.status(400).send(error);
  }

});

// RECUPERATION COMPTE USER
router.get('/:id', (req, res) => {
  res.send('OK');
});

// UPDATE USER ACCOUNT
router.put('/:id', (req, res) => {
  res.send('OK');
});

module.exports = router;