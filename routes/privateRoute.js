const jwt = require("jsonwebtoken");

//Cette fonction verifie que le token passé est bien lié a un utilisateur.
//Cette fonction sert de middleware pour proteger les routes souhaitées
function checkAuth(req, res, next) {
  const token = req.header('auth-token');
  if (!token) return res.status(401).send('Access denied...');

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
  } catch (error) {
    console.log(error);
    req.status(400).send('Invalid token...')
  }

}