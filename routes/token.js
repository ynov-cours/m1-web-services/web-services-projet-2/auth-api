const router = require('express').Router();
const bcrypt = require('bcryptjs');
const User = require('../model/User');
const { createAccessTokenValidation } = require('./validation');
const jwt = require('jsonwebtoken');


//CREATION COMPTE USER
router.post('/token', async (req, res) => {
  //DATA VALIDATION
  const { error } = createAccessTokenValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findOne({ login: req.body.login });
  if (!user) return res.status(400).send("Aucun utilisateur trouvé...");

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send("Mot de passe incorect...");

  const token = jwt.sign({ _id: user.id }, process.env.TOKEN_SECRET);
  res.header('auth-token', token).status(200).send(token);

  //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjQ3MjAxMTBlMTk0OGZmODMzY2IzNTMiLCJpYXQiOjE2NDg4MzEwNzJ9.s7y0QcOt93Mlp1OO1disYRqqN8Wo1EzyKiXTMJ26Btc
});

module.exports = router;