const Joi = require('@hapi/joi');

const registerValidation = (toValidate) => {
  const userSchema = Joi.object({
    login: Joi.string().max(255).required(),
    password: Joi.string().max(1024).min(4).required(),
    roles: Joi.array().required(),
    status: Joi.string().required()
  });

  return userSchema.validate(toValidate)
}

const loginValidation = (toValidate) => {
  const userSchema = Joi.object({
    login: Joi.string().max(255).required(),
    password: Joi.string().max(1024).min(4).required()
  });

  return userSchema.validate(toValidate)
}

const createAccessTokenValidation = (toValidate) => {
  const userSchema = Joi.object({
    login: Joi.string().max(255).required(),
    password: Joi.string().max(1024).min(4).required()
  });

  return userSchema.validate(toValidate)
}

module.exports = { registerValidation, loginValidation, createAccessTokenValidation };